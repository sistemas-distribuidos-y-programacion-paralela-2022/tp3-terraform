provider "aws" {
    region = "eu-west-2"
}

resource "aws_vpc" "myvpc" {
    cidr_block = "10.0.0.0/16"
}

resource "aws_vpc" "myvpc2" {
    cidr_block = "192.168.0.0/24"
}

//Para importar los recursos, luego de haberlos desplegado, o sea como si fuera una infraestructura ya exsitente ejecutar:
// terraform import aws_vpc.myvpc2 <ID>