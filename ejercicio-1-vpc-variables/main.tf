provider "aws" {
    region = "us-east-1"  
}

resource "aws_vpc" "vpc-tp3" {
    cidr_block = "10.0.0.0/16"
    tags = {
        Name = var.inputname
    }
}

//VARIABLES

variable "vpcname" {
  type    = string
  default = "VPC-TP3"
}

variable "sshport" {
  type    = number
  default = 22
}

variable "enabled" {
  default = true
}

variable "mylist" {
  type    = list(string)
  default = ["Value1", "Value2"]
}

variable "mymap" {
  type = map
  default = {
    Key1 = "Value1"
    Key2 = "Value2"
  }
}

variable "inputname" {
  type        = string
  description = "Set the name of the VPC"
}

output "vpcid" {
  value = aws_vpc.vpc-tp3.id
}

variable "mytuple" {
  type    = tuple([string, number, string])
  default = ["cat", 1, "dog"]
}

variable "myobject" {
  type = object({ name = string, port = list(number) })
  default = {
    name = "TJ"
    port = [22, 25, 80]
  }
}
