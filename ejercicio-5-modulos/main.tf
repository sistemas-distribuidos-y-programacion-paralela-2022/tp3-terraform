provider "aws" {
    region = "us-east-1"
}

module "ec2module" {
    source = "./ec2"
    ec2name = "Nombre definido en el main"
}

//Se accede a la variable expuesta en el modulo
output "module_output" {
    value = module.ec2module.instance_id
}