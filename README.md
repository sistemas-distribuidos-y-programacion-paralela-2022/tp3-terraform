# TP3-Terraform

## Comandos 

Para iniciar el directorio creando los archivos, cargando estados remotos, descargando modulos, etc.
- terraform init


Para hacer un plan de lo que pasaria antes de hacer un apply:
- terraform plan

Para deployar el plan:
- terraform apply

Para borrar el despliegue
- terraform delete

# Flujo de trabajo Packer (ejercicio 14)

Usar packer permite personalizar las imagenes AMI y hacer despliegues masivos mas rapidos y consistentes en configuracion en comparacion con hacerlo manualmente como en los ejercicios anteriores.

## Crear una clave 
`
`ssh-keygen -t rsa -C "your_email@example.com" -f ./tf-packer`
`
## Personalizar el archivo setup ubicado en /scripts

Este scripts se ejecuta al momento de buildear la imagen. Son comandos para instalar y configurar la instancia realizando tareas como instalar paquetes, parchear el kernel, crear usuarios, o descargar archivos.


## Configurar en el archivo /images/image.pkr.hcl los parametros de la imagen a construir 


En el bloque "variables" se configura la region, que debe coincidir con el archivo .tf, y se crea un id pbasado en el timestamp para la nueva AMI.

En el bloque source se encuentra la plantilla para la AMI, en este caso se define el recurso Elastic Block Storage, buscando una imagen base que coincida con el filtro t2.micro.

En el bloque provisioner se copia la clave SSH generada en el paso anterior.

## Buldear la imagen con packer build

Para buildear la imagen ejecutar el siguiente comando:

`packer build image.pkr.hcl`

La imagen ya deberia estar disponible en la cuenta AWS configurada.

La salida del comando debera devolver el codigo AMI.

## Deployar la imagen buildeada en Terraform editando /instances/main.tf

Para deployar la imagen buildeada editar el archivo /instances/main.tf estableciendo en el valor del atributo "ami" el AMI generado en el comando anterior. 

## configurar en /instances/tfvars la region

`region = "us-east-1"`

## Deployar la infraestructura creada en Terraform

`terraform init && terraform apply`

La salida del comando es la IP publica de la instancia

##  Para testear la App

### Conectarse via SSH

`ssh terraform@$(terraform output -raw public_ip) -i ../tf-packer`

### Ir al directorio de Go

cd go/src/github.com/hashicorp/learn-go-webapp-demo

### Ejecutar la demo 

`go run webapp.go`

## Al terminar, como siempre, borrar los recursos para evitar costos no deseados

`terraform destroy`

*Fuente: https://learn.hashicorp.com/tutorials/terraform/packer#create-a-local-ssh-key* 

# Para borrar una clave (o informacion sensible) sin dejar rastros:

## Checkout

git checkout --orphan latest_branch

## Add all the files

git add -A

## Commit the changes

git commit -am "commit message"

## Delete the branch

git branch -D main

## Rename the current branch to main

git branch -m main

## Finally, force update your repository

git push -f origin main

PS: this will not keep your old commit history around
