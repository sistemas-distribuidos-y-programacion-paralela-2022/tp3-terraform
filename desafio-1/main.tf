provider "aws" {
    region = "us-east-1"  
}

resource "aws_vpc" "vpc-tp3" {
    cidr_block = "196.168.0.0/24"
    tags = {
        Name = "desafio-1"
    }
}