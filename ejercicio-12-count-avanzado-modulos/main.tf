provider "aws" {
    region = "us-east-1"
}

module "db" {
    source = "./db" //path donde esta el modulo
    server_names = ["mariadb","mysql","mssql"] //lista pasada como parametro al modulo
}

output "private_ips" {
    value = module.db.PrivateIP
}