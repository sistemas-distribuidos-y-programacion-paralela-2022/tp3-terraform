variable "server_names" {
    type = list(string)
}

//Se crea una instancia db por cada item de la lista
resource "aws_instance" "db" {
    ami = "ami-0022f774911c1d690"
    instance_type = "t2.micro"
    count = length(var.server_names)
    tags = {
        Name = var.server_names[count.index]
    }
}

output "PrivateIP" {
    value = [aws_instance.db.*.private_ip]
}