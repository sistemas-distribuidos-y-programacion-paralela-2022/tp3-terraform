provider "aws" {
    region = "us-east-1"
}

//creo una db y muestro la ip privada

resource "aws_instance" "db" {
    ami = "ami-0022f774911c1d690"
    instance_type = "t2.micro"

    tags{
        Name = DB
    }
}

output "IP-DB" {
    value = aws_instance.db.private_ip
}

//Creo un web server y muestro la ip publica

resource "aws_instance" "web-server" {
    ami = "ami-0022f774911c1d690"
    instance_type = "t2.micro"
    security_groups = [aws_security_group.web_traffic.name]
    //Para ejecutar el script en la instancia uso la siguiente propiedad
    user_data = file("server-script.sh")
    tags{
        Name = web-server
    }
}
resource "aws_eip" "web-server-ip" {
    instance = aws_instance.web-server.id
}

output "EIP" {
    value = aws_eip.web-server-ip.public_ip
}

//Creo un Security Group exponiendo puerto 80 y 443 (HHTP y HTTPS)
//Usando un dynamic block con foreach

resource "aws_security_group" "web_traffic" {
    name = "Allow Web Traffic"

    dynamic "ingress" {
        iterator = port
        for_each = var.ingress
        content {
            from_port = port.value
            to_port = port. value
            protocol = "TCP"
            cidr_blocks = ["0.0.0.0/0"]
        }
    }

        dynamic "egress" {
        iterator = port
        for_each = var.egress
        content {
            from_port = port.value
            to_port = port. value
            protocol = "TCP"
            cidr_blocks = ["0.0.0.0/0"]
        }
    }
}