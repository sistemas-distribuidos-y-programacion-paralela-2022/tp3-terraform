resource "aws_instance" "db" {
    ami = "ami-0022f774911c1d690"
    instance_type = "t2.micro"

    tags = {
        Name = "DB Server"
    }
}

output "PrivateIP" {
    value = aws_instance.db.private_ip
}